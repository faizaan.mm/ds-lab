#include<iostream>
//#include<conio.h>
#include<string.h>
#include<ctype.h>
using namespace std;
class stack
{
	private:
		int a[10]; int top;
	public:
	void push(int x);
	int pop();
	int eval(int a, int b, char op);
	int post_eval(char *a);
	stack()
	{
		top=-1;
	}	
	void display();
};
void stack::push(int x)
{
	top++;
	a[top]=x;
}
int stack::pop()
{
	return a[top--];
}
void stack::display()
{
	while(top!=-1)
	{
		pop();
	}
}
int stack::eval(int a, int b , char op)
{
	switch(op)
	{
		case '+': return a+b;
		case '-': return a-b;
		case '*': return a*b;
		case '/': return a/b;
		default : return 0;
	}
}
int stack::post_eval(char a[])
{
	stack s;
	int i; int val;
	char ch;
	for(i=0;i<strlen(a);i++)
	{
		if(isdigit(a[i]))
			{s.push(a[i]-'0');}
		else
		{
			int op1=s.pop();
			int op2=s.pop();
			val=eval(op2,op1,a[i]);
			s.push(val);	
		}
	}
	return s.pop();
}
int main()
{
	//clrscr();
	cout<<"enter postfix \n";
	char s[10]; stack a;
	cin.getline(s,100);
	int val = a.post_eval(s);
	cout<<val;
	return 0;
	//getch();
}