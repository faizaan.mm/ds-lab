#include <iostream>
using namespace std;

class node
{public:
    int data;
    node *left;
    node *right;
    node *create();
    void inorder(node*);
};

int main()
{
    node n;
    cout << "Enter data. Enter -1 at any point to stop.\n";
    node *root = NULL;
    root = n.create();
    n.inorder(root);
    return 0;
}

node* node::create()
{
    int x;
    cin >> x;

    if(x == -1) return NULL;

    node *temp = new node;
    temp->data = x;

    cout << "Left child of " << x << endl;
    temp->left = create();
    cout << "Right child of " << x << endl;
    temp->right = create();

    return temp;
}

void node::inorder(node *root)
{
    if(!root) return;
    inorder(root->left);
    cout << root->data << " " ;
    inorder(root->right);
}
