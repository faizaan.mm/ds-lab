#include<iostream>
#include<math.h>
//#include<conio.h>
#include<ctype.h>
#include<string.h>
using namespace std;
class Stack{
    int top;
    int arr[50];
public:
    void push(int a){
	if(top == 49){
	    cout<<"Stack Overflow";
	}else
	  arr[++top] = a;
    }
    int pop(){
	if(top == -1){
	    cout<<"Stack Underflow";
	}else
	   return arr[top--];
    }
};
int preEval(char p[]){
    Stack st;
    int l = strlen(p);
    for(int i = l-1; i>=0; i--){

	if(p[i]>='0'&&p[i]<='9'){
	    char ch = p[i];
	    int a = (int)ch - 48;
	    st.push(a);
	}else{
	    int a = st.pop();
	    int b = st.pop();
	    switch(p[i]){
		case '+':{
		    st.push(b+a);
		    break;
		}
		case '-':{
		    st.push(a-b);
		    break;
		}
		case '*':{
		    st.push(b*a);
		    break;
		}
		case '/':{
		    st.push(a/b);
		    break;
		}
		case '^':{
		    st.push(pow(b,a));
		    break;
		}
	    }
	}
    }
    return st.pop();
}

int main(){
    //clrscr();
    cout<<"Enter expression : ";
    char str[50];
    cin.get(str,50);
    cout<<preEval(str);
    //getch();
    return 0;
}
