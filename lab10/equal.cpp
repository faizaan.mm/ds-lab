#include <iostream>
using namespace std;

class node
{public:
    int data;
    node *left;
    node *right;
    node *create();
    void inorder(node*);
    int check(node*, node*);
};

int main()
{
    node n;
    cout << "Enter data. Enter -1 at any point to stop.\n";
    node *root = NULL;
    cout<<"tree 1 \n";
    root = n.create();
    n.inorder(root);
    node *root2= NULL;
    cout<<"tree 2 \n";
    root2 = n.create();
    n.inorder(root2);
    int x= n.check(root,root2);
    if(x==1){cout<<"equal";}
    else{cout<<"not equal";}
    return 0;
}

node* node::create()
{
    int x;
    cin >> x;

    if(x == -1) return NULL;

    node *temp = new node;
    temp->data = x;

    cout << "Left child of " << x << endl;
    temp->left = create();
    cout << "Right child of " << x << endl;
    temp->right = create();

    return temp;
}

void node::inorder(node *root)
{
    if(!root) return;
    inorder(root->left);
    cout << root->data << " " ;
    inorder(root->right);
}

int node::check(node *root1, node *root2 )
{
    if(root1==NULL && root2==NULL) 
        return 1;
    if(root1!=NULL && root2!=NULL)
    {
        return(
            root1->data==root2->data &&  check(root1->left,root2->left) && check(root1->right,root2->right) 
            );
    }

    return 0;
}