#include<iostream>

using namespace std;

class node{
public:
    char key;
    node *lc, *rc;
};
node* createNode(char ele){
    node* x = new node;
    x->key = ele;
    x->lc=x->rc=NULL;
    return x;
}
int isOperator(char c){
    if(c=='+'||c=='-'||c=='*'||c=='/')
        return 1;
    return 0;
}

node* createTree(char str[]){
    node *Stack[100];
    int top = -1;
    node *t, *t1, *t2;
    for(int i = 0; str[i]; i++){
        if(!isOperator(str[i])){
            t = createNode(str[i]);
            Stack[++top] = t;
        }else{
            t = createNode(str[i]);
            t1 = Stack[top--];
            t2 = Stack[top--];
            t->rc = t1;
            t->lc = t2;
            Stack[++top] = t;
        }
    }
    t = Stack[top--];
    return t;
}
int eval(node* root)
{
    // empty tree
    if (!root)
        return 0;

    // leaf node i.e, an integer
    if (!root->lc && !root->rc){
        int a = root->key-48;
        return a;
    }

    // Evaluate left subtree
    int l_val = eval(root->lc);

    // Evaluate right subtree
    int r_val = eval(root->rc);

    // Check which operator to apply
    if (root->key=='+')
        return l_val+r_val;

    if (root->key=='-')
        return l_val-r_val;

    if (root->key=='*')
        return l_val*r_val;

    return l_val/r_val;
}

void inorder(node* root){
    if(root==NULL)
        return;
    inorder(root->lc);
    cout<<root->key;
    inorder(root->rc);
}
int main(){
    char post[100];
    cin.get(post,100);
    node* et = createTree(post);
    inorder(et);
    cout<<eval(et);
}
