#include <iostream>
#include <string.h>
using namespace std;

class node
{
public:
    int data;
    node *left;
    node *right;
    node();
    node(int);
    node* create();
    int depth(int);
    int leaves();
    void inorder(node*);
	void postorder(node*);
	void preorder(node*);
	void parent(node*, int);
	int ancestors(node*, int);
};


int main()
{
    node n;
    cout << "Enter data. Enter -1 at any point to stop.\n";
    node *root = NULL;
    root = n.create();

    cout << "\nINORDER: \n";
    root->inorder(root);
    cout << "\nPREORDER: \n";
    root->preorder(root);
    cout << "\nPOSTORDER: \n";
    root->postorder(root);

    cout << "\nThe depth of the tree is " << root->depth(0);

    cout << "\nEnter node data to find parent.\n";
    int x;
    cin >> x;
    root->parent(root, x);

    cout << "\nEnter node data to find all ancestors.\n";
    int y;
    cin >> y;
    root->ancestors(root, y);
    cout << "The tree has " << root->leaves() << " leaves\n";


    return 0;
}

node::node()
{
    data = 0;
    left = right = NULL;
}

node::node(int x)
{
    data = x;
    left = right = NULL;
}

node* node::create()
{
    int x;
    cin >> x;

    if(x == -1) return NULL;

    node *temp = new node;
    temp->data = x;

    cout << "Left child of " << x << endl;
    temp->left = create();
    cout << "Right child of " << x << endl;
    temp->right = create();

    return temp;
}

void node::inorder(node *ptr)
{
    int top=-1;
node *stack[10];

do{
    for(;ptr;ptr=ptr->left)
    stack[++top]=ptr;

    if(top>=0)          
        ptr=stack[top--];
    else                
        break;

    cout<<ptr->data<<" ";
    ptr=ptr->right ;

}while(1);
}

void node::preorder(node *ptr)
{
    int top=-1;
    node *stack[10];

do{
    for(;ptr;ptr=ptr->left)
    {
    cout<<ptr->data<<" ";
    stack[++top]=ptr;
    }
    if(top>=0)          
        ptr=stack[top--];
    else             
       break;
    ptr=ptr->right;}
    while(1);
}

void node::postorder(node *root)
{
    if(!root) return;
    postorder(root->left);
    postorder(root->right);
    cout << root->data << " ";

}

void node::parent(node *root, int x)
{
    if(!root) return;
    if(root->left && root->left->data == x)
    {
        cout << root->data;
        return;
    }
    if(root->right && root->right->data == x)
    {
        cout << root->data;
        return;
    }
    parent(root->left, x);
    parent(root->right, x);
}

int node::ancestors(node *root, int x)
{
    if(!root) return 0;
    if(root->data == x) return 1;
    if(ancestors(root->left, x) || ancestors(root->right, x))
        cout << root->data << " ";
}

int node::depth(int x)
{
    if(!this) return x - 1;
    return max(left->depth(x + 1), right->depth(x + 1));
}

int node::leaves()
{
    if(!this) return 0;
    if(!left && !right) return 1;
    return left->leaves() + right->leaves();
}
