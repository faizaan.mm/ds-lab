#include<iostream>
using namespace std;
class sparse
{
private:
	int r,c,v,t;
public:
	void display(sparse s[])
	{
		int i;
		//cout<<"Sparse matrix \n";
		for(i=0;i<=s[0].v;i++)
		{
			cout<<s[i].r<<"\t"<<s[i].c<<"\t"<<s[i].v<<"\n";
		}
		cout<<"\n";
	}
	void comp(sparse s[],int a[][4],int m, int n)
	{

		int i,j; t=0; 
		for(i=0;i<m;i++)
		{
			for(j=0;j<n;j++)
			{
				if(a[i][j]!=0)
				{
					t++;
					s[t].r=i;
					s[t].c=j;
					s[t].v=a[i][j]; 
				}
			}
		}
		s[0].r=m;
		s[0].c=n;
		s[0].v=t;
		cout<<"Sparse matrix- \n";
		display(s);
		
	}
	void transpose(sparse s[])
	{
		sparse tr[100]; int i,j,k;
		tr[0].r=s[0].c; tr[0].c=s[0].r; tr[0].v=t;
        k=1;
        for(i=0;i<s[0].c;i++)
        {
        	for(j=1;j<=s[0].v;j++)
        	{
        		if(i==s[j].c)
        		{
        			tr[k].r=i;
        			tr[k].c=s[j].r;
        			tr[k].v=s[j].v;
        			k++;	
        		}
        	}
        }
        cout<<"slow transpose matrix- \n";
        display(tr);
		cout<<"\n";
	}
	void ftranspose(sparse s[])
	{
		sparse tr[10]; int i,j;
		int rowt[s[0].c + 1]; int rows[s[0].c + 1];
		tr[0].r=s[0].c;
		tr[0].c=s[0].r;
		tr[0].v=s[0].v;
		for(i=0;i<s[0].c;i++)
		{
			rowt[i]=0;
		}
		for(i=1;i<=s[0].v;i++)
		{
			rowt[s[i].c]++;
		}
		rows[0]=1;
		for(i=1;i<s[0].c;i++)
		{
			rows[i]=rowt[i-1]+rows[i-1];
		}
		for(i=0;i<=s[0].v;i++)
		{
			j=rows[s[i].c];
			tr[j].r=s[i].c;
			tr[j].c=s[i].r;
			tr[j].v=s[i].v;
			rows[s[i].c]++;
		}
		cout<<"Fast transpose matrix- \n";
		display(tr);
		cout<<"\n";
	}
};
int main()
{
	int a[4][4];int i,j;
	cout<<"enter elements \n";
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			cin>>a[i][j];
		}
	}
	sparse s[100];
	s[0].comp(s,a,4,4);
	cout<<"transpose matrix \n";
	s[0].transpose(s);
	s[0].ftranspose(s);
	return 0;
}